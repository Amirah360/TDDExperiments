from unittest import TestCase 

from polynomial import Polynomial

class PolynomialTestSuite(TestCase):
#    def test_create_polynomial(self):
#        self.p1 = Polynomial('2x^2 + 2x + 2')
#        self.assertEqual(str(self.p1), '2x^2 + 2x + 2')
#    
#    def test_polynomial_without_spaces_between_terms(self):
#        self.p2 = Polynomial('7x^3+4x^2+x')
#        self.assertEqual(str(self.p2), '7x^3 + 4x^2 + x')
#
#    def test_polynomial_return_two_tuples_of_coefficents_and_exponents(self):
#        self.p3 = Polynomial('2x^3 + 3x^4')
#        self.assertEqual(Polynomial.get_terms(self.p3), [(2, 3), (3, 4)])
    def test_polynomial_return_coefficents(self):
        self.p4 = Polynomial('3x^4 - 7x^2 + 11')
        self.assertEquals(Polynomial.get_coefficents(self.p4), (11,0,-7,0,3))
