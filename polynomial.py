import re

class Polynomial:
    def __init__(self, poly_str):
        self.poly_str = poly_str
        self.get_terms() 
    
#    def __str__(self):
#        return self.poly_str

    def get_terms(self):
        self.poly_str  = (self.poly_str.replace(' ', ''))
        regex = re.compile(r"([+-]*)(\d+)?(x(\^(\d+))?)?")
        terms = regex.findall(self.poly_str)
        terms.pop()
        self.results = []
        for t in terms:
            if t[1] is '':
                coeff = 1
            else:
                coeff = int(t[1])
            if t[0] == '-':
                    coeff = -coeff
            if t[2] is '':
                exp = 0
            elif t[4] is '':
                exp = 1
            else:
                exp = int(t[4])
            tupel = (coeff, exp)
            self.results.append(tupel)
        return(self.results)
    
    def get_coefficents(self):
        terms = self.get_terms(self.poly_str)
        for i in terms:
            print(i)
