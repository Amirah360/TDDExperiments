def reverse_list(lst):
    """
    Reverses order of elements in list lst.
    """
    reversed = [] 
    for item in lst:
        reversed = [item] + reversed 
    return reversed

def reverse_string(s):
    """
    Reverses order of characters in string s.
    """
    str = ""
    for ch in s:
        str = ch + str 
    return str

def is_english_vowel(c):
    """
    Returns True if c is an english vowel
    and False otherwise.
    """
    vowels = "aeiouyAEIOUY"
    return c in vowels

def count_num_vowels(s):
    """
    Returns the number of vowels in a string s.
    """
    counter = 0 
    for ch in s:
        if is_english_vowel(ch) == True:
            counter += 1 
        else:
            counter += 0 
    return counter

def histogram(l):
    """
    Converts a list of integers into a simple string histogram.
    """
    h = ""
    for i in l: 
        count = i 
        while count != 0:
            h += '#'
            count -= 1 
        if i != l[-1] and count == 0:
            h += "\n"
    return h

def get_word_lengths(s):
    """
    Returns a list of integers representing
    the word lengths in string s.
    """
    lengths = []
    word = ""
    for i in s:
        if i != ' ':
            word += i
        else:
<<<<<<< HEAD
            lengths += len(word)
=======
            lengths = len.append(word) 
>>>>>>> 8657be40417cf523509bade106da068c46961aaf
    for i in words:
        lengths += len(words[i])
    return lengths

