from unittest import TestCase

from python_tdd_exercises import *


class TDDExcerciseSuite(TestCase):

    def test_reverse_list(self):
        self.assertEqual(reverse_list([1, 2, 3, 4, 5]), [5, 4, 3, 2, 1])
    
    def test_reverse_string(self):
        self.assertEqual(reverse_string("foobar"), "raboof")
    def test_is_english_vowel(self):
        self.assertEqual(is_english_vowel('a'), True)
        self.assertEqual(is_english_vowel('e'), True) 
        self.assertEqual(is_english_vowel('i'), True)
        self.assertEqual(is_english_vowel('o'), True)
        self.assertEqual(is_english_vowel('u'), True) 
        self.assertEqual(is_english_vowel('y'), True)
        self.assertEqual(is_english_vowel('A'), True) 
        self.assertEqual(is_english_vowel('E'), True) 
        self.assertEqual(is_english_vowel('I'), True)  
        self.assertEqual(is_english_vowel('O'), True) 
        self.assertEqual(is_english_vowel('U'), True) 
        self.assertEqual(is_english_vowel('Y'), True)
        self.assertNotEqual(is_english_vowel('k'), True)
        self.assertNotEqual(is_english_vowel('?'), True)
        self.assertNotEqual(is_english_vowel('z'), True)
    def test_count_num_vowels(self):
        self.assertEqual(count_num_vowels("hey ho let's go"), 5)
        self.assertEqual(count_num_vowels("HEY ho let's GO"), 5)
        self.assertEqual(count_num_vowels("""She told me her name was Billie Jean,
                   as she caused a scene
                   Then every head turned with eyes
                   that dreamed of being the one
                   Who will dance on the floor in the round"""), 54)
    def test_histogram(self):
        self.assertEquals(histogram([2, 5, 1]), '##\n#####\n#')

    def test_get_word_lengths(self):
        self.assertEquals(get_word_lengths("Three tomatoes are walking down the street"), [5, 8, 3, 7, 4, 3, 6])


